package edu.sjsu.android.exercise4;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.app.SearchManager;
import java.io.File;
import android.os.Environment;
import android.content.pm.*;
import java.util.List;


public class MainActivity extends AppCompatActivity {
    String url = "http://www.amazon.com";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //browser(this);

    }

    public void makeCall(View view) {
        String myPhoneNumberUri ="tel:194912344444";
        Intent myActivity2= new Intent(Intent.ACTION_DIAL, Uri.parse(myPhoneNumberUri));
        startActivity(myActivity2);
    }


    public void webBrowser(View view) {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse(url));

        intent.putExtra("url", url);
        startActivity(Intent.createChooser(intent, "Load \"" + url + "\" with: "));

    }
}

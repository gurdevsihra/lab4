package edu.sjsu.android.exercise4;

import android.content.Intent;
import android.os.Bundle;
import android.webkit.WebView;
import android.widget.TextView;
import java.net.*;
import java.util.*;
import android.util.Log;
import java.io.*;
import android.app.Activity;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import androidx.appcompat.app.AppCompatActivity;

public class MyBrowser extends AppCompatActivity {

    TextView text;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.mybrowser_activity);

        text = (TextView)findViewById(R.id.text);
        Intent intent = getIntent();
        text.setText(intent.getStringExtra("url"));
    }
}
